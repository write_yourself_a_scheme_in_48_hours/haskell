{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Safe #-}

module Main ( main ) where

import Prelude ((++), head, IO, putStrLn)
import System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs
    putStrLn ("Hello, " ++ head args)
