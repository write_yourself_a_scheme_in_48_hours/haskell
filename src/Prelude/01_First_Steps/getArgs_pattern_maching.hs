{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Safe #-}

module Main ( main ) where

import Prelude ((<>), IO, putStrLn)
import System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs
    putStrLn (\case
            [] -> "Usage: hello name"
            (h:_) -> "Hello, " <> h
        args)
