{-# LANGUAGE Safe #-}

module Main ( main ) where

-- import Data.Text (Text)
import Data.Void ( Void )
import Prelude
    ( (++)
    , Char
    , Either( Left, Right )
    , IO
    , putStrLn
    , show
    , String
    )
import System.Environment ( getArgs )
import Text.Megaparsec
    ( choice
    , many
    , parse
    , Parsec
    , parseTest
    )
import Text.Megaparsec.Char ( char )

type Parser = Parsec Void String

symbol :: Parser Char
-- symbol = oneOf "!#$%&|*+-/:<=>?@^_~"
symbol = choice
    [ char '!'
    , char '#'
    ]

spaces :: Parser [Char]
-- spaces :: ParsecT Void String Data.Functor.Identity.Identity [Char]
spaces = many (char ' ')

pLine :: Parser Char
pLine = do
  _ <- spaces
  symbol

readExpr :: String -> String
readExpr input = case parse pLine "lisp" input of
    Left err -> "No match: " ++ show err
    Right _ -> "Found value"

main :: IO ()
main = do 
        (expr:_) <- getArgs
        parseTest pLine expr
        putStrLn (readExpr expr)

-- Adapted from: https://en.m.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours

