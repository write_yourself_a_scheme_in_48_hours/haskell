{-# LANGUAGE Safe #-}

module Main ( main ) where

-- import Control.Monad
-- import Data.Text (Text)
import Data.Void ( Void )
import Prelude
    ( (.), ($), (++), (>>), (<$>)
    , Bool( False, True )
    , Char
    , Either( Left, Right )
    , Integer
    , IO
    , putStrLn
    , read
    , return
    , show
    , String
    )
import System.Environment ( getArgs )
import Text.Megaparsec
    ( (<|>)
    , anySingleBut
    , choice
    , endBy
    , many
    , parse
    , Parsec
    , sepBy
    , try
    )
import Text.Megaparsec.Char
    ( char
    , digitChar
    , letterChar
    )


type Parser = Parsec Void String

data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | String String
             | Bool Bool

symbol :: Parser Char
-- symbol = oneOf "!#$%&|*+-/:<=>?@^_~"
symbol = choice
    [ char '!'
    , char '#'
    ]

spaces :: Parser [Char]
-- spaces :: ParsecT Void String Data.Functor.Identity.Identity [Char]
spaces = many (char ' ')

parseString :: Parser LispVal
parseString = do
                _ <- char '"'
                x <- many (anySingleBut '"')
                _ <- char '"'
                return $ String x

parseAtom :: Parser LispVal
parseAtom = do 
              first <- letterChar <|> symbol
              rest <- many (letterChar <|> digitChar <|> symbol)
              let atom = first:rest
              return $ case atom of 
                         "#t" -> Bool True
                         "#f" -> Bool False
                         _    -> Atom atom

parseNumber :: Parser LispVal
parseNumber = Number . read <$> many digitChar

parseList :: Parser LispVal
parseList = List <$> sepBy parseExpr spaces

parseDottedList :: Parser LispVal
parseDottedList = do
    list_head <- endBy parseExpr spaces
    list_tail <- char '.' >> spaces >> parseExpr
    return $ DottedList list_head list_tail

parseQuoted :: Parser LispVal
parseQuoted = do
    _ <- char '\''
    x <- parseExpr
    return $ List [Atom "quote", x]

parseExpr :: Parser LispVal
parseExpr = parseAtom
         <|> parseString
         <|> parseNumber
         <|> parseQuoted
         <|> do _ <- char '('
                x <- try parseList <|> parseDottedList
                _ <- char ')'
                return x

readExpr :: String -> String
readExpr input = case parse parseExpr "lisp" input of
    Left err -> "No match: " ++ show err
    Right _ -> "Found value"


main :: IO ()
main = do 
        (expr:_) <- getArgs
        -- parseTest parseExpr expr
        putStrLn (readExpr expr)

-- Adapted from: https://en.m.wikibooks.org/wiki/Write_Yourself_a_Scheme_in_48_Hours

