{-# LANGUAGE Safe #-}

module Main ( main ) where

import Prelude
    ( (.), ($), (++), (>>), (<$>)
    , Bool( False, True)
    , Char
    , Either( Left, Right )
    , Integer
    , IO
    , map
    , putStrLn
    , read
    , return
    , Show
    , show
    , String
    , unwords
    )
import System.Environment ( getArgs )
import Text.ParserCombinators.Parsec
    ( (<|>)
    , char
    , digit
    , endBy
    , letter
    , many
    , many1
    , noneOf
    , oneOf
    , parse
    , Parser
    , sepBy
    , skipMany1
    , space
    , try
    )


data LispVal = Atom String
             | List [LispVal]
             | DottedList [LispVal] LispVal
             | Number Integer
             | String String
             | Bool Bool

symbol :: Parser Char
symbol = oneOf "!#$%&|*+-/:<=>?@^_~"

spaces :: Parser ()
spaces = skipMany1 space

parseString :: Parser LispVal
parseString = do
                _ <- char '"'
                x <- many (noneOf "\"")
                _ <- char '"'
                return $ String x

parseAtom :: Parser LispVal
parseAtom = do 
              first <- letter <|> symbol
              rest <- many (letter <|> digit <|> symbol)
              let atom = first:rest
              return $ case atom of 
                         "#t" -> Bool True
                         "#f" -> Bool False
                         _    -> Atom atom

parseNumber :: Parser LispVal
parseNumber = Number . read <$> many1 digit

parseList :: Parser LispVal
parseList = List <$> sepBy parseExpr spaces

parseDottedList :: Parser LispVal
parseDottedList = do
    list_head <- endBy parseExpr spaces
    list_tail <- char '.' >> spaces >> parseExpr
    return $ DottedList list_head list_tail

parseQuoted :: Parser LispVal
parseQuoted = do
    _ <- char '\''
    x <- parseExpr
    return $ List [Atom "quote", x]

parseExpr :: Parser LispVal
parseExpr = parseAtom
         <|> parseString
         <|> parseNumber
         <|> parseQuoted
         <|> do _ <- char '('
                x <- try parseList <|> parseDottedList
                _ <- char ')'
                return x

showVal :: LispVal -> String
showVal (String contents) = "\"" ++ contents ++ "\""
showVal (Atom name) = name
showVal (Number contents) = show contents
showVal (Bool True) = "#t"
showVal (Bool False) = "#f"
showVal (List contents) = "(" ++ unwordsList contents ++ ")"
showVal (DottedList list_head list_tail) = "(" ++ unwordsList list_head ++ " . " ++ showVal list_tail ++ ")"

instance Show LispVal where show = showVal

unwordsList :: [LispVal] -> String
unwordsList = unwords . map showVal

readExpr :: String -> String
readExpr input = case parse parseExpr "lisp" input of
    Left err -> "No match: " ++ show err
    Right val -> "Found " ++ show val

main :: IO ()
main = do 
         (expr:_) <- getArgs
         putStrLn (readExpr expr)

