{-# LANGUAGE Safe #-}

module Main ( main ) where

import Prelude
    ( (++), (>>)
    , Char
    , Either( Left, Right )
    , IO
    , putStrLn
    , show
    , String
    )
import System.Environment ( getArgs )
import Text.ParserCombinators.Parsec
    ( oneOf
    , parse
    , Parser
    , skipMany1
    , space
    )
-- hiding (spaces)


symbol :: Parser Char
symbol = oneOf "!#$%&|*+-/:<=>?@^_~"

spaces :: Parser ()
spaces = skipMany1 space

readExpr :: String -> String
readExpr input = case parse (spaces >> symbol) "lisp" input of
    Left err -> "No match: " ++ show err
    Right _ -> "Found value"

main :: IO ()
main = do 
         (expr:_) <- getArgs
         putStrLn (readExpr expr)

