{-# LANGUAGE Safe #-}

module Main ( main ) where

import Prelude
    ( (++)
    , Char
    , Either( Left, Right )
    , IO
    , putStrLn
    , show
    , String
    )
import System.Environment ( getArgs )
import Text.ParserCombinators.Parsec
    ( oneOf
    , parse
    , Parser
    )
-- hiding (spaces)

symbol :: Parser Char
symbol = oneOf "!#$%&|*+-/:<=>?@^_~"

readExpr :: String -> String
readExpr input = case parse symbol "lisp" input of
    Left err -> "No match: " ++ show err
    Right _ -> "Found value"

main :: IO ()
main = do 
         (expr:_) <- getArgs
         putStrLn (readExpr expr)

