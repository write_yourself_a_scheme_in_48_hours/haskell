{-# LANGUAGE BlockArguments #-}
{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE Trustworthy #-}

module Main ( main ) where

import Basement.Environment
    ( getArgs )

import Foundation
    ( (<>) -- Associative operation on Semigroup
    , IO
    )
import Foundation.IO.Terminal ( putStrLn )


main :: IO ()
main = do
    args <- getArgs
    putStrLn (\case
            [] -> "Usage: hello name"
            (h:_) -> "Hello, " <> h
        args)
