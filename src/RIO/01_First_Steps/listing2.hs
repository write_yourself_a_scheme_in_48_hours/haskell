{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}

module Main where

import RIO ((++)
           , head -- Module ‘RIO’ does not export ‘head’
           , IO
           , putStrLn -- Module ‘RIO’ does not export ‘putStrLn’
           )
import System.Environment (getArgs)

main :: IO ()
main = do
    args <- getArgs
    putStrLn ("Hello, " ++ head args)
