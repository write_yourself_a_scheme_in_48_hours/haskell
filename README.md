# Haskell

# Check used libraries are up to date
* [*State of the Haskell ecosystem*
  ](https://github.com/Gabriel439/post-rfc/blob/master/sotu.md)
  (2020)
* Compare with Idris...

# Similar topics
* [husk-scheme](https://hackage.haskell.org/package/husk-scheme) (2021)
* [*Write You a Haskell*
  ](https://github.com/sdiehl/write-you-a-haskell)
  Building a modern functional compiler from first principles
  (2017) Stephen Diehl
* [*Crafting interpreters*
  ](https://craftinginterpreters.com/)
  A handbook for making programming languages
  2015-2020 Robert Nystrom
* [*Writing A Lisp Interpreter In Haskell*
  ](http://www.defmacro.org/ramblings/lisp-in-haskell.html)
  2006-10
