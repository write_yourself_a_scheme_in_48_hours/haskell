variables: &variables
    CABAL_DIR: $CI_PROJECT_DIR/.cabal
    CONFIG: | # Unused?
        repository hackage.haskell.org
            url: http://hackage.haskell.org/
        install-dirs user
            prefix: $CI_PROJECT_DIR/.cabal
    GHC: "ghc -dynamic"
    GHC_FLAGS: |-
        -dynamic
        -fdiagnostics-color=always
    OCICI_BY_PACKAGES: registry.gitlab.com/oci-container-images-by-packages
    COLUMNS: 50
    TERM: xterm-color

    ALPINE_IMAGE: alpine
    ARCHLINUX_IMAGE: $OCICI_BY_PACKAGES/archlinux/cabal-install__ghc__haskell-megaparsec__stack

.ghc_warnings: &ghc_warnings
    GHC_WARNINGS: |-
        -Werror
        -Weverything
        -Wall
        -Wcompat
        -Widentities
        -Wincomplete-record-updates
        -Wincomplete-uni-patterns
        -Wpartial-fields
        -Wredundant-constraints
        -Wwarn=unsafe
    # https://hackage.haskell.org/package/rio


cache:
    paths:
    - $CABAL_DIR
    policy: pull
# Gain: 5-10 seconds,
# at the price of a one minute second job added, but maybe skipable on condition.


.zeroes-steps: &zeroes-steps # Used?
- ghc --version
- shopt -s expand_aliases || true
- alias ghc='ghc -dynamic' # For Archlinux
- cabal --version
- test -e ${CABAL_DIR:-~/.cabal}/packages || exit "Cache not found."
- echo $GHC_PACKAGE_PATH
- |
  export GHC_PACKAGE_PATH=$(ls -d $CABAL_DIR/store/ghc-*/package.db):$GHC_PACKAGE_PATH
- printenv GHC_PACKAGE_PATH
- echo $CABAL_DIR/store/ghc-*/package.db
- ls --color $CABAL_DIR/store/ghc-*/package.db || true
- ghc-pkg list
- ghc-pkg check

.first-steps: &first-steps
- cd $CI_PROJECT_DIR/src/Prelude/01_First_Steps
- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o hello_you
    getArgs_pattern_maching.hs
- ./hello_you Jonathan
- rm hello_you

- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o hello_you
    listing2.hs
- ./hello_you Jonathan
- rm hello_you

- cd $CI_PROJECT_DIR/src/Foundation/01_First_Steps
- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o hello_you
    getArgs_pattern_maching.hs
  && ./hello_you Jonathan
  && rm hello_you

- cd $CI_PROJECT_DIR/src/RIO/01_First_Steps
- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o hello_you
    listing2.hs
  && ./hello_you Jonathan || true

.megaparsec: &megaparsec
- cd $CI_PROJECT_DIR/src/Prelude/megaparsec/02_Parsing
- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o symbol
    01_symbol.hs
        # -XOverloadedStrings
- ./symbol !
- ./symbol "#"
- ./symbol $
- ./symbol " !"

- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o add
    02_add.hs
        # -XOverloadedStrings
- ./add !
- ./add "#"
- ./add $
- ./add " !"
- ./add " "

- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o return_values
    03_return_values.hs
        # -XOverloadedStrings
- ./return_values !
- ./return_values "#"
- ./return_values $
- ./return_values " !"
- ./return_values " "
- ./return_values "\"this is a string\""
- ./return_values 25
- ./return_values symbol
- ./return_values "(symbol)"

- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o return_values
    04_recursive_parsers.hs
        # -XOverloadedStrings
- ./return_values !
- ./return_values "#"
- ./return_values $
- ./return_values " !"
- ./return_values " "
- ./return_values "\"this is a string\""
- ./return_values 25
- ./return_values symbol
- ./return_values "(symbol)"
- ./return_values "(a test)"
- ./return_values "(a (nested) test)"
- ./return_values "(a (dotted . list) test)"
- ./return_values "(a '(quoted (dotted . list)) test)"
- ./return_values "(a '(imbalanced parens)"

.parsec: &parsec
- cd $CI_PROJECT_DIR/src/Prelude/parsec/02_Parsing
- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o simple_parser
    listing3_1.hs
- ./simple_parser $
- ./simple_parser a
- ./simple_parser " $"
- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o simple_parser
    listing3_2.hs
- ./simple_parser $
- ./simple_parser " $"
- ./simple_parser a
- ./simple_parser " a"
- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o simple_parser
    listing3_3.hs
- ./simple_parser $
- ./simple_parser " $"
- ./simple_parser a
- ./simple_parser " a"
- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o simple_parser
    listing3_4.hs
- ./simple_parser "(a test)"
- ./simple_parser "(a (nested) test)"
- ./simple_parser "(a (dotted . list) test)"
- ./simple_parser "(a '(quoted (dotted . list)) test)"
- ./simple_parser "(a '(imbalanced parens)"

- cd $CI_PROJECT_DIR/src/Prelude/parsec/03_Evaluation,_Part_1
- ghc 
        --make
        ${GHC_FLAGS}
        ${GHC_WARNINGS:-}
        -o parser
    listing4_1.hs
- ./parser "(1 2 2)"
- ./parser "'(1 3 (\"this\" \"one\"))"
# - ghc 
#         --make
#         ${GHC_FLAGS}
#         ${GHC_WARNINGS:-}
#         -o eval
#     listing4_2.hs
# - ./eval "'atom" 
# - ./eval 2
# - ./eval "\"a string\""
# - ( ! ./eval "(+ 2 2)" )
# - rm eval
# - ghc 
#         --make
#         ${GHC_FLAGS}
#         ${GHC_WARNINGS:-}
#         -o eval
#     listing4_3.hs
# - ./eval "(+ 2 2)"
# - ./eval "(+ 2 (-4 1))"
# - ./eval "(+ 2 (- 4 1))"
# - ./eval "(- (+ 4 6 3) 3 5 2)"

- cd $CI_PROJECT_DIR/src/Prelude/parsec/04_Error_Checking_and_Exceptions
# - ghc 
#         --make
#         ${GHC_FLAGS}
#         ${GHC_WARNINGS:-}
#         -o errorcheck
#     listing5.hs
# - ./errorcheck "(+ 2 \"two\")"
# - ./errorcheck "(+ 2)"
# - ./errorcheck "(what? 2)"

- cd $CI_PROJECT_DIR/src/Prelude/parsec/05_Evaluation,_Part_2
# - ghc 
#         --make
#         ${GHC_FLAGS}
#         ${GHC_WARNINGS:-}
#         -o simple_parser
#     listing6_1.hs
# - ./simple_parser "(< 2 3)"
# - ./simple_parser "(> 2 3)"
# - ./simple_parser "(>= 3 3)"
# - ./simple_parser "(string=? \"test\"  \"test\")"
# - ./simple_parser "(string<? \"abc\" \"bba\")"
# - ghc 
#         --make
#         ${GHC_FLAGS}
#         ${GHC_WARNINGS:-}
#         -o simple_parser
#     listing6_2.hs
# - ./simple_parser "(if (> 2 3) \"no\" \"yes\")"
# "yes"
# - ./simple_parser "(if (= 3 3) (+ 2 3 (- 5 1)) \"unequal\")"
# 9

._hlint: # Used?
    script:
    - hlint 05_Evaluation,_Part_2/listing6_1.hs
    image: $OCICI_BY_PACKAGES/opensuse-tumbleweed/busybox__hlint

.hlint: &hlint
    script:
    - hlint --version
    - hlint $CI_PROJECT_DIR
    # - hlint hello.hs

.pacman_plain: &pacman_plain
 before_script:
    - (. /etc/os-release ; echo $PRETTY_NAME) || true
    - ${CHRONIC:-} ${SUDO:-} pacman
            --sync
            --refresh
            --noconfirm
            --color always
            --quiet
        elinks
        ghc
        haskell-foundation
        haskell-megaparsec
        haskell-parsec
        haskell-rio

.pacman_ghc: &pacman_ghc
 before_script:
    - (. /etc/os-release ; echo $PRETTY_NAME) || true
    - ${CHRONIC:-} ${SUDO:-} pacman
            --sync
            --refresh
            --noconfirm
            --color always
            --quiet
        elinks
        haskell-foundation
        haskell-megaparsec
        haskell-parsec
        haskell-rio

.pacman-hlint: &pacman-hlint
 before_script:
    - (. /etc/os-release ; echo $PRETTY_NAME)
    - ${CHRONIC:-} ${SUDO:-} pacman
            --sync
            --refresh
            --noconfirm
            --color always
            --quiet
        hlint


archlinux:
    <<: *pacman_plain
    script:
    - *first-steps
    - *megaparsec
    - *parsec
    image: archlinux

archlinux-warnings:
    <<: *pacman_plain
    script:
    - *first-steps
    - *megaparsec
    - *parsec
    variables:
        <<: *ghc_warnings
        <<: *variables
    image: archlinux
    allow_failure: true

archlinux-hlint:
    <<: *hlint
    <<: *pacman-hlint
    image: archlinux
    allow_failure: true

archlinux_by:
    <<: *pacman_ghc
    script:
    - *first-steps
    - *megaparsec
    - *parsec
    image: $OCICI_BY_PACKAGES/archlinux/awk__ghc__moreutils__pacman


###########################################

.archlinux-cached:
    script:
    - *first-steps
    - *megaparsec
    - *parsec
    cache:
        key: archlinux
        paths:
        - $CABAL_DIR
        policy: pull
    image: $ARCHLINUX_IMAGE

# archlinux-parsec:
    # haskell-parsec pacman package seems not to exist 2020-08
    # See: archlinux-cached

.alpine:
    script:
    - *first-steps
    - *megaparsec
    - *parsec
    before_script: &apk
    - printenv CABAL_DIR
    - time apk add --update-cache --quiet
        cabal
        curl
        ghc
        musl-dev
    cache:
        key: alpine
        paths:
        - $CABAL_DIR
        policy: pull
    image: $ALPINE_IMAGE

.cache-job: &cache-job
    stage: .pre
    rules:
    - if: '$PUSH_CACHE'
    - if: '$CI_PIPELINE_SOURCE == "schedule"'
    script:
    - time cabal new-update
    - ghc-pkg check
    - time cabal new-install
            --disable-library-vanilla
            --enable-shared
            --enable-executable-dynamic
            --ghc-options=-dynamic
        megaparsec
        parsec
    - ls -A --color $CABAL_DIR
    - du -sch $CABAL_DIR/*

.cache-job-alpine:
    <<: *cache-job
    before_script:
    - *apk
    cache:
        key: alpine
        policy: push
        paths:
        - $CABAL_DIR
    image: $ALPINE_IMAGE

.cache-job-archlinux:
    <<: *cache-job
    cache:
        key: archlinux
        policy: push
        paths:
        - $CABAL_DIR
    image: $ARCHLINUX_IMAGE
    # As megaparsec is on the image, Cabal does not compile it.
